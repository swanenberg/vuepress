module.exports = {
    title: 'Drinks@home docs',
    description: 'Full documentation on the drinks@home platform',
    base: '/vuepress/',
    dest: 'public',

    themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'External', link: 'https://google.com' },
    ],

    sidebar: 'auto',
  }
}
