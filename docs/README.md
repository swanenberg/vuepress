---
home: true
heroImage: /hero.png
features:
- title: Simplicity First
  details: Here we show you the complete documentation of the Drinkies platform
- title: Working together on the documentation is great
  details: so...
- title: Performant
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2018-present Evan You
---